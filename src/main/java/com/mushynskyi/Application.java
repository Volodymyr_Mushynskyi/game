package com.mushynskyi;

import com.mushynskyi.view.View;

/**
 * @author Volodymyr Mushynskyi
 * @version 1.0.0
 * @since 2019-09-08
 */
public class Application {
  /**
   * This is the main method which start game.
   *
   * @param args Unused.
   */
  public static void main(String[] args) throws InterruptedException {
    new View().printMenu();
  }
}
