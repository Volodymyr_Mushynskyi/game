package com.mushynskyi.model.gamemanger;

public interface Model {
  void shoot(int row, int col, int colForDroid);

  void moveYourDroid(int row, int col);

  void printListOfBattleField() throws InterruptedException;

  void printVictoryInGame();

  Integer getPoints();

}
