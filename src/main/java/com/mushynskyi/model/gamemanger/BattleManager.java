package com.mushynskyi.model.gamemanger;

import com.mushynskyi.constant.Configuration;
import com.mushynskyi.model.battel.Battle;
import com.mushynskyi.model.battel.BattleField;
import com.mushynskyi.model.droids.Droid;
import com.mushynskyi.model.droids.GamerDroid;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.mushynskyi.model.battel.BattleField.battleField;
import static com.mushynskyi.model.battel.BattleField.listOfEnemyDroids;

public class BattleManager implements Model {

  private Droid enemyDroid;
  private Droid gamerDroid;
  private Battle battle;
  public int point;

  public static String string = "";
  public static List<String> list = new ArrayList<>();

  public BattleManager() {
    battle = new BattleField();
    gamerDroid = new GamerDroid(Configuration.POINTS, Configuration.HEALTH, Configuration.GAMER_DROID_ICON, Configuration.ROW, Configuration.COLUMN);
  }

  private void fire(Integer start, Integer end) {
    string = "";
    for (int i = 0; i < 10; i++) {
      for (int j = 0; j < 10; j++) {
        if (j == end && i == start) {
          if (battleField[i][j].contains(Configuration.ENEMY_ICON)) {
            enemyDroid = findTarget(i, j);
            enemyDroid.decreaseHealthOfDroid();
            if (enemyDroid.getHealth() == 0) {
              battleField[i][j] = Configuration.EXPLOSIVE_ICON;
              gamerDroid.addPoints(point++);
            }
          } else {
            battleField[i][j] = Configuration.BULLET;
          }
        }
        if (!battleField[i][j].contains(Configuration.GAMER_DROID_ICON)) {
          if (j == end && i == start + 1) {
            battleField[i][j] = Configuration.DEFAULT_FIELD;
          }
        }
        string += battleField[i][j];
      }
      string += "\n";
    }
    string += "\n";
    list.add(string);
  }

  private Droid findTarget(Integer row, Integer column) {
    Optional<Droid> enemyDroids = listOfEnemyDroids.stream()
            .filter(droid -> (droid.getRow().equals(row) && droid.getColumn().equals(column)))
            .findAny();
    return enemyDroid = enemyDroids.get();
  }

  @Override
  public void shoot(int k, int col, int colForDroid) {
    if (colForDroid == col) {
      for (int i = gamerDroid.getRow() - 1; i > k - 1; i--) {
        fire(i, col);
      }
    } else {
      System.out.println("You have not move your ship on position for shooting");
    }
  }

  @Override
  public void moveYourDroid(int row, int colForDroid) {
    battleField[row][colForDroid] = Configuration.GAMER_DROID_ICON;
  }

  @Override
  public void printListOfBattleField() throws InterruptedException {
    for (String stateOfField : list) {
      System.out.print(stateOfField);
      Thread.sleep(100);
    }
    list.clear();
  }

  @Override
  public Integer getPoints() {
    return gamerDroid.getPoints();
  }

  @Override
  public void printVictoryInGame() {
    if (gamerDroid.getPoints() == 40) {
      System.out.print("------------------------\n"
              + "|      YOU WIN !!!     |\n"
              + "|       GAME OVER      |\n"
              + "------------------------\n");
    }
  }
}
