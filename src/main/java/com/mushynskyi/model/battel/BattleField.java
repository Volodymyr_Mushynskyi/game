package com.mushynskyi.model.battel;

import com.mushynskyi.constant.Configuration;
import com.mushynskyi.model.droids.Droid;
import com.mushynskyi.model.droids.EnemyDroid;

import java.util.ArrayList;
import java.util.List;

public class BattleField implements Battle {

  public final static String[][] battleField = new String[10][10];

  public final static List<Droid> listOfEnemyDroids = new ArrayList<>();

  public BattleField() {
    createBattleField();
    createListOfEnemyDroids();
  }

  @Override
  public void createBattleField() {
    for (int i = 0; i < 10; i++) {
      for (int j = 0; j < 10; j++) {
        battleField[i][j] = Configuration.DEFAULT_FIELD;
      }
    }
    createCoordinates();
  }

  private void createCoordinates() {
    for (int i = 0; i < 10; i++) {
      for (int j = 0; j < 10; j++) {
        if (i == 0) {
          battleField[i][j] = String.valueOf(j) + "  ";
        }
        if (j == 0) {
          battleField[i][j] = String.valueOf(i) + "  ";
        }
      }
    }
  }

  @Override
  public void printBattleField() {
    for (int i = 0; i < 10; i++) {
      for (int j = 0; j < 10; j++) {
        System.out.print(battleField[i][j]);
      }
      System.out.println();
    }
    System.out.println();
  }

  @Override
  public void setCustomField(int row, int col) {
    battleField[row][col] = Configuration.DEFAULT_FIELD;
  }

  @Override
  public void createListOfEnemyDroids() {
    listOfEnemyDroids.add(new EnemyDroid(Configuration.ENEMY_HEALTH, Configuration.ENEMY_ICON, 4, 5));
    listOfEnemyDroids.add(new EnemyDroid(Configuration.ENEMY_HEALTH, Configuration.ENEMY_ICON, 2, 3));
    listOfEnemyDroids.add(new EnemyDroid(Configuration.ENEMY_HEALTH, Configuration.ENEMY_ICON, 1, 9));
    listOfEnemyDroids.add(new EnemyDroid(Configuration.ENEMY_HEALTH, Configuration.ENEMY_ICON, 1, 4));
  }
}
