package com.mushynskyi.model.battel;

public interface Battle {
  void createBattleField();

  void printBattleField();

  void setCustomField(int row, int col);

  void createListOfEnemyDroids();
}
