package com.mushynskyi.model.droids;

import static com.mushynskyi.model.battel.BattleField.battleField;

public class GamerDroid implements Droid {

  private Integer points;
  private Integer health;
  private String iconOfDroid;
  private Integer row;
  private Integer column;

  public GamerDroid() {

  }

  public GamerDroid(Integer points, Integer health, String iconOfDroid, Integer row, Integer y) {
    this.points = points;
    this.health = health;
    this.iconOfDroid = iconOfDroid;
    this.row = row;
    this.column = y;
    battleField[row][y] = iconOfDroid;
  }

  public Integer getRow() {
    return row;
  }

  public Integer getColumn() {
    return column;
  }

  @Override
  public void decreaseHealthOfDroid() {
  }

  @Override
  public void addPoints(int i) {
    this.points = getPoints() + 10;
  }

  public Integer getPoints() {
    return points;
  }

  public Integer getHealth() {
    return health;
  }

  public void setHealth(Integer health) {
    this.health = health;
  }

  public String getIconOfDroid() {
    return iconOfDroid;
  }

  public void setIconOfDroid(String iconOfDroid) {
    this.iconOfDroid = iconOfDroid;
  }
}
