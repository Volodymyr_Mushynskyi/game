package com.mushynskyi.model.droids;

public interface Droid {
  void decreaseHealthOfDroid();

  Integer getHealth();

  void addPoints(int i);

  Integer getPoints();

  Integer getRow();

  Integer getColumn();
}
