package com.mushynskyi.model.droids;

import static com.mushynskyi.model.battel.BattleField.battleField;

public class EnemyDroid implements Droid {
  private Integer enemyHealth;
  private String iconOfDroid;
  private Integer row;
  private Integer column;

  public EnemyDroid() {
  }

  public EnemyDroid(Integer enemyHealth, String iconOfDroid, Integer row, Integer column) {
    this.enemyHealth = enemyHealth;
    this.iconOfDroid = iconOfDroid;
    this.row = row;
    this.column = column;
    battleField[row][column] = iconOfDroid;
  }

  @Override
  public Integer getHealth() {
    return enemyHealth;
  }

  @Override
  public void addPoints(int i) {

  }

  @Override
  public Integer getPoints() {
    return 0;
  }

  @Override
  public void decreaseHealthOfDroid() {
    this.enemyHealth = this.enemyHealth - 10;
  }

  public void setEnemyHealth(Integer enemyHealth) {
    this.enemyHealth = enemyHealth;
  }

  public String getIconOfDroid() {
    return iconOfDroid;
  }

  public void setIconOfDroid(String iconOfDroid) {
    this.iconOfDroid = iconOfDroid;
  }

  public Integer getRow() {
    return row;
  }

  public Integer getColumn() {
    return column;
  }
}
