package com.mushynskyi.controller;

import com.mushynskyi.model.battel.Battle;
import com.mushynskyi.model.battel.BattleField;
import com.mushynskyi.model.gamemanger.BattleManager;
import com.mushynskyi.model.gamemanger.Model;

public class Controller implements IController {
  private Model model;
  private Battle battle;

  public Controller() {
    model = new BattleManager();
    battle = new BattleField();
  }

  @Override
  public void shoot(int row, int column, int colForDroid) {
    model.shoot(row, column, colForDroid);
  }

  @Override
  public void moveYourDroid(int row, int colForDroid) {
    model.moveYourDroid(row, colForDroid);
  }

  @Override
  public void printListOfBattleField() throws InterruptedException {
    model.printListOfBattleField();
  }

  @Override
  public void setCustomField(int row, int col) {
    battle.setCustomField(row, col);
  }

  @Override
  public void printBattleField() {
    battle.printBattleField();
  }

  @Override
  public void printVictoryInGame() {
    model.printVictoryInGame();
  }

  @Override
  public Integer getPoints() {
    return model.getPoints();
  }
}
