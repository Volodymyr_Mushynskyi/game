package com.mushynskyi.controller;

public interface IController {
  void shoot(int row, int col, int colForDroid);

  void moveYourDroid(int row, int col);

  void setCustomField(int row, int col);

  void printListOfBattleField() throws InterruptedException;

  void printBattleField();

  void printVictoryInGame();

  Integer getPoints();
}
