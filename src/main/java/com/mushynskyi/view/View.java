package com.mushynskyi.view;

import com.mushynskyi.constant.Configuration;
import com.mushynskyi.controller.Controller;
import com.mushynskyi.controller.IController;
import com.mushynskyi.model.gamemanger.BattleManager;

import java.util.Scanner;

public class View {
  private final IController controller;

  private final Scanner scanner = new Scanner(System.in);

  public View() {
    controller = new Controller();
  }

  public void printMenu() throws InterruptedException {
    byte choose;
    int row;
    int col;
    int colForDroid = 9;

    BattleManager battleManager = new BattleManager();

    controller.printBattleField();
    while (true) {
      controller.printVictoryInGame();
      System.out.println("----------------------MENU----------------------------");
      System.out.println("Press 1 Print your points");
      System.out.println("Press 2 Shoot");
      System.out.println("Press 3 Move your ship");
      System.out.println("Press 0 Exit");
      choose = scanner.nextByte();
      switch (choose) {
        case 1:
          System.out.println("Your points : " + controller.getPoints());
          break;
        case 2:
          System.out.println("Write coordinates of your target x and y");
          row = scanner.nextInt();
          col = scanner.nextInt();
          controller.shoot(row, col, colForDroid);
          controller.printListOfBattleField();
          controller.shoot(row, col, colForDroid);
          controller.printListOfBattleField();
          break;
        case 3:
          controller.setCustomField(Configuration.ROW, colForDroid);
          System.out.println("Write your coordinates, only y");
          colForDroid = scanner.nextInt();
          controller.moveYourDroid(Configuration.ROW, colForDroid);
          controller.printBattleField();
          break;
        case 0:
          return;
        default:
      }
    }
  }
}
