package com.mushynskyi.constant;

public interface Configuration {
  Integer POINTS = 0;
  Integer ROW = 9;
  Integer COLUMN = 9;
  Integer HEALTH = 100;
  String GAMER_DROID_ICON = "¥  ";
  String BULLET = "|  ";
  String EXPLOSIVE_ICON = "☼  ";
  String ENEMY_ICON = "¤  ";
  Integer ENEMY_HEALTH = 20;
  String DEFAULT_FIELD = "O  ";
}
